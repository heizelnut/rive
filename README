rive
====

implementation of a secret sharing system using XOR


idea
----

following https://en.wikipedia.org/wiki/Secret_sharing#t_=_n

0. let ^ be the bitwise XOR operation
1. let S be the secret to distribute, encoded as a binary number
2. given N (number of shares), generate N-1 random numbers
	r[1] = rand(), r[2] = rand(), r[3] = rand(), ... r[N-1] = rand()
3. set r[N] = S ^ r[1] ^ r[2] ^ r[3] ^ ... r[N-1]
4. distribute r[1] to r[N] as shares and keep S secret
5. as a consequence we have S = r[1] ^ r[2] ^ r[3] ^ ... r[N]


usage
-----

	./rive N		# generate N shares from the secret text passed as stdin
	./rive			# join newline-separated shares passed as stdin


installation
------------

read the Makefile before compiling on Linux / BSDs.


example
-------
	
	# do not printf the plain text secret into stdin, as this may save your
	#	secret sauce into your shell history and may be seen from
	#	your process tree.

	$ printf "super secret sauce recipe" | ./rive 5 | tee shares.txt
	rive:71304C4A4F5B706745263B3D77384E273B5D484E3652435C22$
	rive:68747C63432A297C504D4B4A32246A32225A4D726E53643576$
	rive:25757E2D4029686F204071292539534C4478602D7C7927725D$
	rive:625C6B5E225B33647D34437C20725D5B5320393D6E2A5C4142$
	rive:2D18553F1C2371752B6D275660244B776D3A7C5E2F31352A2E$

	# of couse you should delete the shares.txt file and distribute the shares to
	#    your trusted parties.

	# after the parties reunite their shares, they can get the secret back with:

	$ ./rive < shares.txt
	super secret sauce recipe


disclaimer
----------

I AM NOT a cryptographer nor an expert in any kind, I made this for leisure and
learning purposes. If you find some bugs or vulnerabilities, you can send a
patch or a pull request. I am not liable in any way if you choose to use this
software.


license
-------

theoretically it's ISC Licensed, but practically do whatever you want with it

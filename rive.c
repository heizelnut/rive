/* rive - deriving secret shares using XOR
 
 ISC License

 Copyright 2023 heizelnut

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>	
#ifdef __linux__
#include <bsd/stdlib.h>	
#endif
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_SECRET_SIZE 1000
#define E_INVALID -2

#ifdef HAVE_ARC4RANDOM
#define RANDFUNC arc4random
#else
#define RANDFUNC rand
#endif

static char *alphabet = \
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"1234567890"
	"@#$%&-+()*\"':;!?^={}\\[]|`~ ";

/* fill str with random characters from global alphabet */
void random_fill(char *str, size_t len)
{
	for (uint8_t i = 0; i < len; i++)
		str[i] = alphabet[RANDFUNC() % strlen(alphabet)];
}

/* store character-wise xor between dst and src into dst */
void strnxor(char *dst, char *src, size_t len)
{
	for (uint8_t i = 0; i < len; i++)
		dst[i] ^= src[i];
}

/* pretty print the share as a hexadecimal string in the `rive:...$` format */
void print_share(char *src, size_t len)
{
	printf("rive:");
	for (uint8_t i = 0; i < len; i++) {
		printf("%02X", (uint8_t)src[i]);
	}
	printf("$\n");
}

/* parse the `rive:...$` format and return the share's length */
size_t parse_share(unsigned char** dst)
{
	static char line[MAX_SECRET_SIZE*2+6];
	static char hshare[MAX_SECRET_SIZE*2];
	static unsigned char share[MAX_SECRET_SIZE];
	
	if (NULL == fgets(line, MAX_SECRET_SIZE*2+6, stdin)) return EOF;
	if (1 != sscanf(line, "rive:%[^$^\n]", hshare)) return E_INVALID;
	size_t hlen = strlen(hshare);
	if (hlen % 2 != 0) return E_INVALID;
	size_t slen = hlen / 2;
	for (uint8_t i = 0; i < slen; i++)
		if (1 != sscanf(&hshare[i*2], "%2hhX", &share[i])) return E_INVALID;
	*dst = share;
	return slen;
}

int main(int argc, char **argv)
{
#ifndef HAVE_ARC4RANDOM
	srand(time(NULL));
#endif
	if (argc == 2) {
		/* share generation mode */
		uint32_t shares = atoi(argv[1]);
		if (shares < 2) return E_INVALID;

		static char secret[MAX_SECRET_SIZE];

		if (NULL == fgets(secret, MAX_SECRET_SIZE, stdin)) return EOF;
		size_t len = strlen(secret);
		if (secret[len-1] == '\n') len--;

		char ubershare[len+1];
		strncpy(ubershare, secret, len);

		for (uint32_t i = 0; i < shares-1; i++) {
			char share[len+1];
			random_fill(share, len);
			strnxor(ubershare, share, len);
			print_share(share, len);
		}
		print_share(ubershare, len);
	} else {
		/* share joining mode */
		unsigned char *share = NULL;
		size_t len = parse_share(&share);
		if (len == E_INVALID || len == EOF) return len;
		unsigned char secret[len];
		strncpy((char*)secret, (char*)share, len);
		size_t next = parse_share(&share);
		while (next != EOF) {
			if (next == EOF) break;
			if (len != next || next == E_INVALID) return E_INVALID;
			strnxor((char*)secret, (char*)share, len);
			next = parse_share(&share);
		}
		printf("%.*s\n", (unsigned int)len, secret);
	}

	return 0;
}

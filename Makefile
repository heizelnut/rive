# if you're using *BSD, change to bsd.makefile
include linux.makefile

TARG = rive
OBJ = rive.o
CFLAGS = -pedantic -Werror -Wall -std=c99

CFLAGS += -DHAVE_ARC4RANDOM # comment out this line to use standard rand() / srand()

.c.o: 
	$(CC) $(CFLAGS) -c $< -o $@

$(TARG): $(OBJ)
	$(CC) $(OBJ) $(LFLAGS) -o $(TARG)

clean:
	rm $(OBJ) $(TARG)
